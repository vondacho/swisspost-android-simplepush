package ch.post.it.notifica.android.simplepush;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gcm.GCMRegistrar;

/**
 * Created by olivier on 11.10.16.
 */

public class GCMRegistrarTaskFactory {
    public static AsyncTask<Void, Void, String> create(final Context context) {
        return new AsyncTask<Void, Void, String>() {
            @Override
            protected String doInBackground(Void... params) {
                try {
                    GCMRegistrar.checkDevice(context);
                    GCMRegistrar.checkManifest(context);
                    String registrationId = GCMRegistrar.getRegistrationId(context);
                    if (registrationId.equals("")) {
                        Log.i("info", "registration at GCM launched with sender id " + Utils.SENDER_ID);
                        GCMRegistrar.register(context, Utils.SENDER_ID);
                    } else {
                        Log.i("info", "already registered at GCM as " + registrationId);
                    }
                    return registrationId;
                }
                catch (Throwable t) {
                    Log.i("info", "unable to register at GCM because " + t.getMessage());
                    return null;
                }
            }

            @Override
            protected void onPostExecute(String registrationId) {
                if (registrationId == null) {
                    Toast.makeText(context, "The registration at GCM has failed", Toast.LENGTH_SHORT).show();
                }
                else if (!registrationId.isEmpty()) {
                    Log.i("info", "registered at GCM as " + registrationId);
                }
            }
        };
    }
}
