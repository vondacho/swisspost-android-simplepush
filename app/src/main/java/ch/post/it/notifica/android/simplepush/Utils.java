package ch.post.it.notifica.android.simplepush;

import android.content.Context;
import android.content.SharedPreferences;

/**
 * Created by olivier on 10.10.16.
 */

public class Utils {
    public static final String SENDER_ID = "414306044877";
    public static final String SHARD_PREF = "ch.post.it.notifica.android.simplepush_preferences";
    public static final String GCM_TOKEN = "gcmtoken";

    public static void saveToken(Context context, String token) {
        SharedPreferences appPrefs = context.getSharedPreferences(Utils.SHARD_PREF, Context.MODE_PRIVATE);
        SharedPreferences.Editor prefsEditor = appPrefs.edit();
        prefsEditor.putString(Utils.GCM_TOKEN, token);
        prefsEditor.apply();
    }

    public static String retrieveToken(Context context) {
        SharedPreferences appPrefs = context.getSharedPreferences(Utils.SHARD_PREF, Context.MODE_PRIVATE);
        return appPrefs.getString(Utils.GCM_TOKEN, "");
    }
}
