package ch.post.it.notifica.android.simplepush;

/**
 * Created by olivier on 10.10.16.
 */

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import com.google.android.gcm.GCMBaseIntentService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Set;

public class GCMIntentService extends GCMBaseIntentService {

    @Override
    protected void onMessage(Context context, Intent intent) {
        JSONObject jsonObject = new JSONObject();
        Set<String> keys = intent.getExtras().keySet();
        for (String key : keys) {
            try {
                jsonObject.put(key, intent.getExtras().get(key));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        try {
            sendNotification(jsonObject.toString(5));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private void sendNotification(final String msg) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                MainActivity.showMessage(msg);
            }
        });
    }

    @Override
    protected void onError(Context context, String s) {

    }

    @Override
    protected void onRegistered(Context context, String token) {
        if (token != null && !token.isEmpty()) {
            Log.i("info", "got registration id " + token + " from GCM");
            Utils.saveToken(context, token);
            sendToken(token);
        }
    }

    private void sendToken(final String token) {
        Handler handler = new Handler(Looper.getMainLooper());
        handler.post(new Runnable() {
            @Override
            public void run() {
                MainActivity.showToken(token);
            }
        });
    }

    @Override
    protected void onUnregistered(Context context, String s) {

    }
}
