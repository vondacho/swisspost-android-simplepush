package ch.post.it.notifica.android.simplepush;

import android.content.Context;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private final Context mContext = this;

    private static TextView mTokenView;
    private static TextView mMessagePayloadView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mMessagePayloadView = (TextView) findViewById(R.id.messagePayloadView);
        mTokenView = (TextView) findViewById(R.id.tokenView);

        if (Utils.retrieveToken(this).isEmpty())
            register(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        showToken(Utils.retrieveToken(this));
    }

    public static void showMessage(String message) {
        mMessagePayloadView.setText(message);
    }

    public static void showToken(String token) {
        mTokenView.setText(token);
    }

    private void register(final Context context) {
        GCMRegistrarTaskFactory.create(context).execute();
    }
}
